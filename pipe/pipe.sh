#!/usr/bin/env bash
# Copyright (C) 2020  IMAGIN Sp. z o.o.
# Author: Marcin Engelmann <mengelmann@octivi.com>

# Unofficial Bash "Strict Mode" http://redsymbol.net/articles/unofficial-bash-strict-mode/
set -euo pipefail
IFS=$'\n\t'

# Required parameters
FTP_USER=${FTP_USER:?'FTP_USER variable missing.'}
FTP_PASSWORD=${FTP_PASSWORD:?'FTP_PASSWORD variable missing.'}
FTP_SERVER=${FTP_SERVER:?'FTP_SERVER variable missing.'}
LOCAL_PATH=${LOCAL_PATH:?'LOCAL_PATH variable missing.'}

git ftp push -v --auto-init --user "${FTP_USER}" --passwd "${FTP_PASSWORD}" --syncroot "${LOCAL_PATH}" "${FTP_SERVER}"
