FROM octivi/git-ftp:1.6.0-alpine3.11

ENTRYPOINT ["/pipe.sh"]

COPY pipe/pipe.sh LICENSE /
